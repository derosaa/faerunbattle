package test;

import Model.Plateau.Castle;
import utils.UtilsCastle;

import java.util.Scanner;


public class TestCastle {
    public static void main(String[] args) {
        //création du chateau
        Castle MinasTirith = new Castle();
        System.out.println("Tour 1 bleu :");
        MinasTirith.choixDuGuerrier();
        System.out.println();
        System.out.println(MinasTirith.getRessource());
        MinasTirith.entrainement();
        MinasTirith.ajoutRessourceFin();
        UtilsCastle.affichageCastle(MinasTirith);


        System.out.println("Tour 1 rouge :");
        Castle BarakDur = new Castle();
        BarakDur.choixDuGuerrier();
        System.out.println();
        System.out.println(BarakDur.getRessource());
        BarakDur.entrainement();
        BarakDur.ajoutRessourceFin();
        UtilsCastle.affichageCastle(BarakDur);


    }




}
