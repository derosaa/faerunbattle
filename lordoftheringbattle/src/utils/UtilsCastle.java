package utils;

import Model.Plateau.Castle;

public class UtilsCastle {

    /**
     * Affichage de toutes les données du castle
     * @param castle
     */
    public static void affichageCastle(Castle castle){
        System.out.println("Ressources du Chateau : "+castle.getRessource());
        System.out.println("Troupes en formation :");
        AffichageFormation(castle);
        System.out.println("Troupes prête à combattre :");
        AffichageArmée(castle);
    }

    /**
     * Affichage des troupes dans le bataillon du castle
     * @param castle
     */
    public static void AffichageArmée(Castle castle){
        if (castle.getCampEntrainement().size() == 0){
            System.out.println("Aucune troupe dans le bataillon");
        }else{
            for(int i=0; i<castle.getBataillon().size(); i++){
                System.out.println(castle.getBataillon().get(i).getClass().getSimpleName());
            }
        }


    }

    /**
     * Affichage des troupes en formation dans le castle
     * @param castle
     */
    public static void AffichageFormation(Castle castle){
        if (castle.getCampEntrainement().size() == 0){
            System.out.println("Aucune troupe en formation");
        }else{
            for(int i=0; i<castle.getCampEntrainement().size(); i++){
                System.out.println(castle.getCampEntrainement().get(i).getClass().getSimpleName());
            }
        }

    }
}
