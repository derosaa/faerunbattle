package utils;

import Model.ModelGuerrier.*;

public class UtilsWarriors {
    // Méthode d'affichage

    /**
     * affichage du guerrier avec ses stats
     * @param guerrier
     */
    public static void printGuerrier(Guerrier guerrier){
        System.out.print(guerrier.getClass().getSimpleName()+ "[HP = "+guerrier.getHealthPoint() +"]");
    }

    /**
     * affichage (saut de ligne) du guerrier avec ses stats
     * @param guerrier
     */
    public static void printlnGuerrier (Guerrier guerrier){
        printGuerrier(guerrier);
        System.out.println();
    }

    /**
     * Affichage des données des combats
     * @param attaquant
     * @param degatReel
     * @param degatPotantiels
     * @param defenseur
     */
    public static void CombatGuerrier(Guerrier attaquant, int degatReel, int degatPotantiels, Guerrier defenseur){
        System.out.println("Combat lancé:");
        printGuerrier(attaquant);
        System.out.print(" attaque (dégats données : "+degatPotantiels+ " ; dégats subits :"+degatReel+ ") ");
        printlnGuerrier(defenseur);
        if (!defenseur.estVivant()){
            printlnGuerrierMort(attaquant, defenseur);
        }

    }

    /**
     * Affichage des morts après un combat
     * @param attaquant
     * @param defenseur
     */
    public static void printlnGuerrierMort(Guerrier attaquant, Guerrier defenseur){
        printGuerrier(defenseur);
        System.out.print(" MORT : a été tué par ");
        printlnGuerrier(attaquant);

    }

}
