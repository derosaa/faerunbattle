package utils;

import Model.ModelGuerrier.*;
import Model.Plateau.Carreaux;
import Model.Plateau.Plateau;

import java.util.HashMap;
import java.util.Random ;
public class UtilsPlateform {
    private static final Random RANDOM = new Random();

    public static int De3() {
        return RANDOM.nextInt(3)+1;
    }

    public static int De3(int nombreLances) {
        int somme = 0;
        for (int i = 0; i < nombreLances; i++) {
            somme = somme + De3();
        }
        return somme;
    }

    public static void affichagePlateau(Plateau plateau){
        HashMap<Integer, Carreaux> tabCarreau = plateau.getCouloir();
        int emplacement = 1;
        for (Carreaux carreau : tabCarreau.values()){
            System.out.print("Case n°"+emplacement);
            System.out.print(" ---- Nombre de guerrier bleu : "+carreau.getBleu().size());
            System.out.println(" ---- Nombre de guerrier rouge : "+carreau.getRouge().size());
            emplacement++;
        }
    }


}
