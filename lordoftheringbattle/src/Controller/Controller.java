package Controller;


import Model.Plateau.Carreaux;
import Model.Plateau.Castle;
import Model.Plateau.Plateau;
import utils.UtilsCastle;

import static java.lang.Thread.sleep;
import static utils.UtilsPlateform.affichagePlateau;


public class Controller {
    private Plateau plateau;
    private Castle Mordor = new Castle();
    private Castle Rohan = new Castle();

    public Controller(){
        plateau = new Plateau();
    }

    public void loop()  {
        plateau.initialisationMap();
        while (!isEnd()){
            phaseFormation(Rohan, Mordor);
            envoiBataillon(plateau, Rohan, Mordor);
            affichagePlateau(plateau);
            phaseDeplacement(plateau);
            phaseDeCombat(plateau);
            if (isWinRouge()){
                System.out.println("Victoire des rouges !!");
            }else if(isWinBLeu()){
                System.out.println("Victoire des bleus !!");
            }

        }
    }
    /**
     * conditions de fin de partie
     */
    public boolean isEnd(){
        return isWinBLeu() || isWinRouge();
    }

    /**
     * condition de win pour les rouges
     * @return
     */
    public boolean isWinBLeu(){
        return plateau.getCouloir().get(plateau.taillePlateau()-1).getBleu().size() > 0 && plateau.getCouloir().get(plateau.taillePlateau()-1).getRouge().size() == 0;
    }

    /**
     * condition de win pour les rouges
     * @return
     */
    public boolean isWinRouge(){
        return plateau.getCouloir().get(0).getRouge().size() > 0 && plateau.getCouloir().get(0).getBleu().size() == 0;
    }

    /**
     * Gestion des déplacements
     * @param plateau
     */
    public void phaseDeplacement(Plateau plateau){
        plateau.deplacementEquipeBleu();
        plateau.deplacementEquipeRouge();
        affichagePlateau(plateau);
    }

    /**
     * gestion des combats
     * @param plateau
     */
    public void phaseDeCombat(Plateau plateau){
        int nbCombat = 0;

        System.out.println("Phase de combat lancée :");
        for (Carreaux carreau : plateau.getCouloir().values()){
            if(carreau.lancerCombat()){
                carreau.phaseCombatBLeu();
                carreau.phaseCombatRouge();
                nbCombat++;
            }
        }
        if (nbCombat == 0){
            System.out.println("Aucun combat n'a eu lieu");
        }
        System.out.println("Fin de la phase de combat");

    }

    /**
     * gestion de la phase de formation dans les chateaux
     * @param castleBleu
     * @param castleRouge
     */
    public void phaseFormation(Castle castleBleu, Castle castleRouge){
        //gestion de la formation pour les bleus
        System.out.println("Tour des bleus : ");
        castleBleu.choixDuGuerrier();
        System.out.println();
        System.out.println(castleBleu.getRessource());
        castleBleu.entrainement();
        castleBleu.ajoutRessourceFin();
        UtilsCastle.affichageCastle(castleBleu);
        // gestion de la formation pour les rouges
        System.out.println("Tour des rouges : ");
        castleRouge.choixDuGuerrier();
        System.out.println();
        System.out.println(castleRouge.getRessource());
        castleRouge.entrainement();
        castleRouge.ajoutRessourceFin();
        UtilsCastle.affichageCastle(castleRouge);
    }

    /**
     * envoie les troupes formées sur le terrain
     * @param plateau
     * @param castleBleu
     * @param castleRouge
     */
    public void envoiBataillon(Plateau plateau, Castle castleBleu, Castle castleRouge){
        envoiBataillonBleu(plateau, castleBleu);
        envoiBataillonRouge(plateau, castleRouge);
    }

    /**
     * envoie des troupes bleus sur le terrain
     * @param plateau
     * @param castle
     */
    public void envoiBataillonBleu(Plateau plateau, Castle castle){
        if(castle.getBataillon().size() != 0) {
            plateau.getCouloir().get(0).getBleu().addAll(castle.getBataillon());
            castle.getBataillon().clear();
        }

    }

    /**
     * envoie des troupes rouges sur le terrain
     * @param plateau
     * @param castle
     */
    public void envoiBataillonRouge(Plateau plateau, Castle castle){
        if(castle.getBataillon().size() != 0) {
            plateau.getCouloir().get(plateau.taillePlateau() - 1).getRouge().addAll(castle.getBataillon());
            castle.getBataillon().clear();
        }
    }



}
