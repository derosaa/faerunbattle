package Model.Plateau;

import Model.ModelGuerrier.*;

import java.util.LinkedList;
import java.util.Scanner;

import static utils.UtilsWarriors.printGuerrier;

public class Castle {
    private static final int INITIAL_RESSOURCES = 3;
    private static final int RESSOURCES_AJOUTEES_PAR_TOUR = 1;
    private LinkedList campEntrainement = new LinkedList();
    private int ressource;
    private LinkedList<Guerrier> bataillon = new LinkedList();


    public Castle(){
        ressource = INITIAL_RESSOURCES;
        LinkedList<Guerrier> troupeFormation = campEntrainement;
        LinkedList<Guerrier> troupePrete = bataillon;
    }

    public LinkedList<Guerrier> getBataillon() {
        return bataillon;
    }

    public void setBataillon(LinkedList<Guerrier> bataillon) {
        this.bataillon = bataillon;
    }

    public LinkedList<Guerrier> getCampEntrainement(){
        return campEntrainement;
    }

    public void setCampEntrainement(LinkedList<Guerrier> campEntrainement){
        this.campEntrainement = campEntrainement;
    }

    public int getRessource(){
        return ressource;
    }

    public void setRessource(int ressource){
        this.ressource = ressource;
    }

    public void entrainerGuerrier(Guerrier guerrier){
        getCampEntrainement().add(guerrier);
        printGuerrier(guerrier);
        System.out.println(" est en entrainement");

    }

    /**
     * Gestion de la formation du castle
     * @param guerrier
     * @param oldSection
     * @param nouvelleSection
     */
    public void formationFini(Guerrier guerrier, LinkedList<Guerrier> oldSection, LinkedList<Guerrier> nouvelleSection){
        nouvelleSection.add(guerrier);
        oldSection.removeFirst();
        int updateRessource = getRessource()-guerrier.getCout();
        setRessource(updateRessource);
        printGuerrier(guerrier);
        System.out.println(" a fini la formation");
        setBataillon(nouvelleSection);
        setCampEntrainement(oldSection);

    }

    /**
     * Gestion de l'entrainement dans le castle
     */
    public void entrainement(){
        boolean formation = true;
        while (getCampEntrainement().size() > 0 && formation) {
            //Formation de guerrier tant qu'il reste des guerrier et qu'il reste assez de ressources
            Guerrier recrue = getCampEntrainement().getFirst();
            if (recrue.getCout() > getRessource()) {
                formation = false;
                System.out.println("formation indisponible : plus assez ressources");
            } else {
                formationFini(recrue, getCampEntrainement(), getBataillon());
            }
        }
    }

    /**
     * Ajout d'une ressource à la fin du tour
     */
    public void ajoutRessourceFin(){
        int ressourceUp = getRessource() + RESSOURCES_AJOUTEES_PAR_TOUR;
        setRessource(ressourceUp);
    }

    /**
     * Permet au joueur de gérer la création des guerriers
     */
    public void choixDuGuerrier(){
        boolean stop = false;
        while (campEntrainement.size() <= 4 && !stop){
            System.out.println("Choisissez un guerrier en sélectionnant son numéro :");
            System.out.println("1:Nain, 2:Elfe, 3:Chef Nain, 4:Chef Elfe, 5: Quittez");
            Scanner num = new Scanner(System.in);
            int i = num.nextInt();
            if (i == 5){
                stop = true;
            }else{
                creationGuerrier(i);
            }


        }
    }

    /**
     * Choix du guerrier à entrainer
     * @param id
     */
    public void creationGuerrier(int id){
        switch (id){
            case 1 :  entrainerGuerrier(new Dwarf());
            break;
            case 2:   entrainerGuerrier(new Elf());
            break;
            case 3:   entrainerGuerrier(new DwarfChief());
            break;
            case 4:   entrainerGuerrier(new ElfChief());
            break;
            default:  entrainerGuerrier(new Dwarf());
            break;
        }

    }



}
