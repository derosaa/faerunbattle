package Model.Plateau;

import Model.ModelGuerrier.Guerrier;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import static utils.UtilsPlateform.affichagePlateau;


public class Plateau {

    private HashMap<Integer, Carreaux> Couloir = new HashMap<Integer, Carreaux>();

    public Plateau(){
        HashMap<Integer, Carreaux> MapMonde = Couloir ;
    }

    public HashMap<Integer, Carreaux> getCouloir() {
        return Couloir;
    }

    public void setCouloir(HashMap<Integer, Carreaux> couloir) {
        Couloir = couloir;
    }

    public int taillePlateau(){
        return getCouloir().size();
    }

    /**
     * Initialisation du champ de bataille par le joueur
     */
    public void initialisationMap(){

        HashMap<Integer, Carreaux>InitMap = getCouloir();
        System.out.println ("Choississez la taille du champ de bataille : ");
        Scanner recupNum = new Scanner(System.in);
        int nbCarreau = recupNum.nextInt();
        for(int i=0; i < nbCarreau; i++){
            InitMap.put(i, new Carreaux());
        }
        setCouloir(InitMap);
        System.out.println("Initialisation réussi");
    }

    /**
     * gère le déplacement de l'équipe bleu
     */
    public  void deplacementEquipeBleu(){
        ArrayList<Guerrier> deplacementBleu  ;
        System.out.println("Déplacement de l'équipe bleu en cours");
        for (int i = getCouloir().size()-1; i >= 0; i--){
            //parcours du couloir
            if(getCouloir().get(i).getBleu().size() > 0 && getCouloir().get(i).getRouge().size() == 0){
                // déplace tout les guerriers bleu s'il sont sur la case et s'il n'y a pas de rouge dessus
                deplacementBleu = getCouloir().get(i).getBleu();
                getCouloir().get(i + 1).getBleu().addAll(deplacementBleu);
                deplacementBleu.clear();
            }
        }


    }

    /**
     * gère le déplacement de l'equipe rouge sur le plateau
     */
    public void deplacementEquipeRouge(){
        ArrayList<Guerrier> deplacementRouge;
        System.out.println("Déplacement de l'équipe rouge en cours");
        for (int i = 0; i < getCouloir().size(); i++){
            //parcours du couloir
            if(getCouloir().get(i).getRouge().size() > 0 && getCouloir().get(i).getBleu().size() == 0){
                // déplace tout les guerriers rouge s'il sont sur la case et s'il n'y a pas de bleu dessus
                deplacementRouge = getCouloir().get(i).getRouge();
                getCouloir().get(i - 1).getRouge().addAll(deplacementRouge);
                deplacementRouge.clear();
            }
        }
    }
















}
