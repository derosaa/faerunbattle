package Model.Plateau;

import Model.ModelGuerrier.Guerrier;
import com.sun.source.tree.MethodInvocationTree;

import java.util.ArrayList;

public class Carreaux {
    private ArrayList<Guerrier> Bleu = new ArrayList<>();
    private ArrayList<Guerrier> Rouge = new ArrayList<>();

    public Carreaux(){
        ArrayList<Guerrier> equipeBleu = Bleu;
        ArrayList<Guerrier> equipeRouge = Rouge;
    }

    public ArrayList<Guerrier> getBleu() {
        return Bleu;
    }

    public ArrayList<Guerrier> getRouge() {
        return Rouge;
    }

    public void setBleu(ArrayList<Guerrier> bleu) {
        Bleu = bleu;
    }

    public void setRouge(ArrayList<Guerrier> rouge) {
        Rouge = rouge;
    }

    public boolean lancerCombat(){
        return getBleu().size() > 0 && getRouge().size() > 0;
    }

    /**
     * gestion combat pour equipe bleu
     */

    public void phaseCombatBLeu(){
        System.out.println("Offensive des bleu");
        try {
            for (int i = 0; i < getBleu().size(); i++) {
                if (getRouge().size() != 0) {
                    getBleu().get(i).attaquer(getRouge().get(0));
                    if (!getRouge().get(0).estVivant()) {
                        getRouge().remove(0);
                    }
                }
            }
        }catch (ArithmeticException e){
            System.out.print("ODIN EST AVEC VOUS GUERRIER : ");
            System.out.println("Coup DIVIN !!!!");
            getRouge().clear();
        }
    }

    /**
     * gestion des combats pour equipe rouge
     */
    public void phaseCombatRouge(){
        System.out.println("Offensive des rouges");
        try {
            for (int i = 0; i < getRouge().size(); i++) {
                if (getBleu().size() != 0) {
                    getRouge().get(i).attaquer(getBleu().get(0));
                    if (!getBleu().get(0).estVivant()) {
                        getBleu().remove(0);
                    }
                }
            }
        }catch (ArithmeticException e){
            System.out.print("ODIN EST AVEC VOUS GUERRIER : ");
            System.out.println("Coup DIVIN !!!!");
            getBleu().clear();
        }
    }



}
