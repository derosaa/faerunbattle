package Model.ModelGuerrier;

import utils.UtilsPlateform;
import utils.UtilsWarriors;

public abstract class Guerrier {
    private static final int INITIAL_HP = 100;
    private static final int INITIAL_STRENGHT = 10 ;

    private int healthPoint ;
    private static final int INITIAL_COST_RESSOURCES = 1;

   public Guerrier() {healthPoint = INITIAL_HP;}

    public int getHealthPoint() {
        return healthPoint;
    }

    public int getStrength() {
        return INITIAL_STRENGHT;
    }

    public int getCout() {
        return INITIAL_COST_RESSOURCES ;
    }

    public void setHealthPoint(int healthPoint) {
        if(healthPoint < 0){
            this.healthPoint = 0;
        }else{
            this.healthPoint = healthPoint;
        }
    }

    public boolean estVivant(){
        return getHealthPoint() > 0;
    }

    public void attaquer (Guerrier guerrier){
        int degatsPotentiels = UtilsPlateform.De3(getStrength());

        if (degatsPotentiels >= 3*getStrength()*0.95){
            throw new ArithmeticException();
        }else {

            int degatsReels = guerrier.subirDegats(degatsPotentiels);

            UtilsWarriors.CombatGuerrier(this, degatsReels, degatsPotentiels, guerrier);
        }

    }

    protected int subirDegats(int degats){
        setHealthPoint(getHealthPoint() - degats);
        return degats;
    }
}
