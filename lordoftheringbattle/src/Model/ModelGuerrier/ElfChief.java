package Model.ModelGuerrier;

public class ElfChief extends Elf {
    private static final int BoostElfChief = 2;
    private static final int RessourceElfChief = 2;
    @Override
    public int getStrength() {
        return super.getStrength()*BoostElfChief;
    }

    @Override
    public int getCout() {
        return super.getCout()*RessourceElfChief;
    }
}
