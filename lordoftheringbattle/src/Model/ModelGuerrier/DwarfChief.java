package Model.ModelGuerrier;

public class DwarfChief extends Dwarf {
    private static final int BoostDwarfChief = 2;
    private static final int RessourceDwarfChief = 3;
    @Override
    protected int subirDegats(int degats) {
        int vraiDegat = degats/BoostDwarfChief;
        super.subirDegats(vraiDegat);
        return vraiDegat;
    }

    @Override
    public int getCout() {
        return super.getCout()*RessourceDwarfChief;
    }
}
