package Model.ModelGuerrier;

public class Dwarf extends Guerrier {
    private static final int BoostResistance = 2;
    @Override
    protected int subirDegats(int degats) {
        int degatReel = degats/BoostResistance;
        super.subirDegats(degatReel);
        return degatReel;
    }
}
