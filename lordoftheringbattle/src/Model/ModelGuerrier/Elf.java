package Model.ModelGuerrier;

public class Elf extends Guerrier {
    private static final int BoostDamage = 2 ;
    private static final int RessourcesElf = 2;

    @Override
    public int getStrength() {
        return super.getStrength()*BoostDamage;
    }

    @Override
    public int getCout() {
        return super.getCout()*RessourcesElf;
    }
}
